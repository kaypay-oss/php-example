<?php

use KayPay\Sdk\Signer;

require __DIR__ . '/../vendor/autoload.php';

$data = '';
if ($argc > 1) {
    $data = $argv[1];
}

$key = '';
if ($argc > 2) {
    $key = $argv[2];
}

$signer = new Signer($key);

echo $signer->signData($data), "\n";
